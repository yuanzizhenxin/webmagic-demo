package com.sxw.github.webmagicdemo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class HouseInfo {

    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String house_type;
    @Column(nullable = false)
    private double house_size;
    @Column(nullable = false)
    private Date create_time;
    @Column(nullable = false)
    private String village_name;
    @Column(nullable = false)
    private String village_site;
    @Column(nullable = false)
    private double pre_price;
    @Column(nullable = false)
    private double total_price;
    @Column(nullable = false)
    private String floor_info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHouse_type() {
        return house_type;
    }

    public void setHouse_type(String house_type) {
        this.house_type = house_type;
    }

    public double getHouse_size() {
        return house_size;
    }

    public void setHouse_size(double house_size) {
        this.house_size = house_size;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public String getVillage_site() {
        return village_site;
    }

    public void setVillage_site(String village_site) {
        this.village_site = village_site;
    }

    public double getPre_price() {
        return pre_price;
    }

    public void setPre_price(double pre_price) {
        this.pre_price = pre_price;
    }

    public double getTotal_price() {
        return total_price;
    }

    public void setTotal_price(double total_price) {
        this.total_price = total_price;
    }

    public String getFloor_info() {
        return floor_info;
    }

    public void setFloor_info(String floor_info) {
        this.floor_info = floor_info;
    }
}
