package com.sxw.github.webmagicdemo.webmagic;

import com.sxw.github.webmagicdemo.model.HouseInfo;
import com.sxw.github.webmagicdemo.repository.FangHouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.List;
import java.util.Map;

@Component
public class FangPipeline implements Pipeline {
    @Autowired
    private FangHouseRepository fangHouseRepository;
    @Override
    public void process(ResultItems resultItems, Task task) {
        Map map = resultItems.getAll();
        List<HouseInfo> houseInfos = (List) map.get("houseInfos");
        for(HouseInfo houseInfo : houseInfos) {
            fangHouseRepository.save(houseInfo);
        }
    }
}
