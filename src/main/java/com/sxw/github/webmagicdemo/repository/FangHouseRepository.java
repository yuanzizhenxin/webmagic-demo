package com.sxw.github.webmagicdemo.repository;

import com.sxw.github.webmagicdemo.model.HouseInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FangHouseRepository extends JpaRepository<HouseInfo,Long> {

    @Override
    <S extends HouseInfo> S save(S s);
}
