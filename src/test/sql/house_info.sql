/*
Navicat MySQL Data Transfer

Source Server         : dockerMysql
Source Server Version : 50723
Source Host           : 192.168.216.131:3306
Source Database       : sys

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2018-11-11 19:52:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for house_info
-- ----------------------------
DROP TABLE IF EXISTS `house_info`;
CREATE TABLE `house_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `house_type` varchar(100) DEFAULT NULL,
  `house_size` double(4,0) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `village_name` varchar(200) DEFAULT NULL,
  `village_site` varchar(200) DEFAULT NULL,
  `pre_price` double DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `floor_info` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6154 DEFAULT CHARSET=utf8;
